#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define MemSize 25

template<class ValType>
class TSimpleStack
{
protected:
	ValType Mem[MemSize];
	int Top;
public:
	TSimpleStack() { Top = -1; }
	TSimpleStack(const ValType&St):Top(St.Top)
	{
		for (int i = 0;i < Top;++i)
			Mem[i] = St.Mem[i];
	}
	bool IsEmpty(void)const { return Top == -1; }
	bool IsFull(void)const { return Top==MemSize-1; }

	void Put(const ValType Val)
	{
		if (IsFull())
			throw 1;
		else
			Mem[++Top] = Val;
	}

	ValType Get(void)
	{
		if (IsEmpty())
			throw 2;
		else
			return Mem[Top--];
	}
};


#endif
