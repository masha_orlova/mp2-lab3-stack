#ifndef __T_DAT_STACK__
#define __T_DAT_STACK__

#include "tdataroot.h"

class TStack : public TDataRoot
{
private:
	int top;

public:
	TStack(int Size = DefMemSize) : TDataRoot(Size), top(-1) { };
	virtual void Put(const TData &Val);
	virtual TData Get();

	virtual int IsValid();
	virtual void Print();
};

#endif