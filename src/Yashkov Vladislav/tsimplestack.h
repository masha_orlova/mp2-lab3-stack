#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define MemSize   255  // ������ ����� �� ���������

template<class ValType>
class TSimpleStack
{
private:
	int top;
	ValType Mem[MemSize];
public:
	TSimpleStack() { top = -1; }
	void Put(const ValType &v);
	ValType Pop();
	bool IsEmpty() const { return top == -1; }
	bool IsFull() const { return top == MemSize - 1; }
};

template <class ValType>
void TSimpleStack<ValType>::Put(const ValType &v)
{
	if (IsFull())
		throw "���� ����������";
	else
	{
		Mem[++top] = v;
	}
}

template <class ValType>
ValType TSimpleStack <ValType> ::Pop()
{
	if (IsEmpty())
		throw "���� ����";
	else
		return Mem[top--];
}

#endif