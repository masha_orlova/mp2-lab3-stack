#include "arithmetic_expression.h"

#include <gtest/gtest.h>

TEST(arithmetic_expression, can_control_if_incom_string_correct)
{
	EXPECT_FALSE(ExpressionControl("(1+2')-3"));
}

TEST(arithmetic_expression, can_control_brackets)
{
	EXPECT_TRUE(BracketsControl("(1+2)-3"));
}

TEST(arithmetic_expression, can_control_missed_brackets)
{
	EXPECT_FALSE(BracketsControl("(3-1"));
}

TEST(arithmetic_expression, can_control_many_brackets)
{
	EXPECT_TRUE(BracketsControl("((3+2)*8+(4-1))/(15-4)"));
}

TEST(arithmetic_expression, can_turn_to_postfix_form)
{
	EXPECT_EQ(" 1 2 +  4 3 - /", PostfixForm("(1+2)/(4-3)"));
}

TEST(arithmetic_expression, can_calculate_postfix_form)
{
	EXPECT_EQ(3, ExpressionRes(PostfixForm("(1+2)/(4-3)")));
}

TEST(arithmetic_expression, can_calculate_large_expression)
{
	EXPECT_EQ((1 + 2) / (3 + 4 * 6.7) - 5.3 * 4.4, ExpressionRes(PostfixForm("(1+2)/(3+4*6.7)-5.3*4.4")));
}