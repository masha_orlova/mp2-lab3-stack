#include "arithmetic_expression.h"

bool ExpressionControl(const string& IncomStr)
{
	for (string::const_iterator it = IncomStr.begin(); it != IncomStr.end(); ++it)
		if (*it < '(' || *it > '9')
			return false;
	return true;
}

bool BracketsControl(const string& IncomStr)
{
	TStack Brackets;
	int Err = 0, BrIndex = 0;
	bool result = true;
	if (!ExpressionControl(IncomStr))
		throw "Incorrect experssion";
	for (string::const_iterator it = IncomStr.begin(); it != IncomStr.end(); ++it)
	{
		if (*it == '(')
		{
			BrIndex++;
			Brackets.Put((TData)BrIndex);
		}
		if (*it == ')')
		{
			BrIndex++;
			if (Brackets.IsEmpty())
			{
				Err++;
				cout << "-  " << BrIndex << endl;
			}
			else
				cout << Brackets.Get() << "  " << BrIndex << endl;
		}
	}
	while (!Brackets.IsEmpty())
	{
		cout << Brackets.Get() << "  -" << endl;
		Err++;
	}
	if (Err > 0)
		result = false;
	return result;
}

int OperatorPriority(char Op)
{
	switch (Op)
	{
	case '(': return 0;
	case ')': return 1;
	case '+': return 2;
	case '-': return 2;
	case '*': return 3;
	case '/': return 3;
	default: return -1;
	}
}

bool IsOperator(char IncomSymb)
{
	if (IncomSymb == '(' || IncomSymb == ')' || IncomSymb == '+' || IncomSymb == '-' || IncomSymb == '*' || IncomSymb == '/')
		return true;
	else
		return false;
}

string PostfixForm(const string& IncomStr)
{
	string PostfixStr;
	TStack Op;
	if (!BracketsControl(IncomStr))
		throw "Incorrect arithmetic expression";
	for (string::const_iterator it = IncomStr.begin(); it != IncomStr.end(); ++it)
	{
		if (!IsOperator(*it))
			PostfixStr.push_back(*it);
		else
		{
			PostfixStr.push_back(' ');
			if (OperatorPriority(*it) == 0 || Op.IsEmpty() || OperatorPriority(*it) > OperatorPriority(Op.GetTopElem()))
				Op.Put(*it);
			else if (*it == ')')
			{
				while (Op.GetTopElem() != '(')
					PostfixStr.push_back((char)Op.Get());
				Op.Get();
			}
			else
			{
				while (!Op.IsEmpty() && OperatorPriority(*it) <= OperatorPriority(Op.GetTopElem()))
					PostfixStr.push_back((char)Op.Get());
				Op.Put(*it);
			}
		}
	}
	PostfixStr.push_back(' ');
	while (!Op.IsEmpty())
		PostfixStr.push_back((char)Op.Get());
	return PostfixStr;
}

TData ExpressionRes(const string& PostfixStr)
{
	TStack Num;
	string tmp;
	TData num1 = 0, num2 = 0;
	for (string::const_iterator it = PostfixStr.begin(); it != PostfixStr.end(); ++it)
	{
		if (IsOperator(*it))
		{
			num2 = Num.Get();
			num1 = Num.Get();
			if (*it == '+')
				Num.Put(num1 + num2);
			else if (*it == '-')
				Num.Put(num1 - num2);
			else if (*it == '*')
				Num.Put(num1 * num2);
			else if (*it == '/')
				Num.Put(num1 / num2);
		}
		else if (*it != ' ')
			tmp.push_back(*it);
		else if (*it == ' ' && tmp != "")
		{
			Num.Put(atof(tmp.c_str()));
			tmp = "";
		}
	}
	return Num.Get();
}

TData Calculate(const string& IncomStr)
{
	return ExpressionRes(PostfixForm(IncomStr));
}
