#ifndef __STACK_H__
#define __STACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
protected:
	int Top;
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { Top = -1; }
	TStack(const TStack &);
	void Put(const TData &);
	TData Get();
	int  IsValid();
	void Print();
	TData GetTopElem();
};

#endif