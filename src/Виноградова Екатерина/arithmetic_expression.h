#include "tstack.h"
#include <iostream>
#include <string>

using namespace std;

bool ExpressionControl(const string& IncomStr);  //Проверка на недопустимые символы
bool BracketsControl(const string& IncomStr);    // Проверка расстановки скобок
int OperatorPriority(char);                      // Присвиваем приоритет операциям
bool IsOperator(char);
string PostfixForm(const string& IncomStr);      // Перевод в постфиксную форму
TData ExpressionRes(const string& PostfixStr);   // Вычисление по постфиксной форме
TData Calculate(const string& IncomStr);
