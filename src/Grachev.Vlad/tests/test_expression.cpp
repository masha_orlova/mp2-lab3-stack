 #define USE_TESTS

#ifdef USE_TESTS

#include <gtest/gtest.h>
#include "expressions.h"

TEST(BrControl, can_check_parentheses_placement)
{
	EXPECT_EQ(true, BrControl("(1+2)/(3+4*6.7)-5.3*4.4", false));
}

TEST(BrControl, can_detect_incorrect_placement_of_parentheses)
{
	EXPECT_EQ(false, BrControl("(a+b1)/2+6.5)*(4.8+(", false));
}

TEST(ConvertToPostfix, can_convert_an_expression_with_int_nums)
{
	EXPECT_EQ(ConvertToPostfix("3*(10-5)+7"), "3 10 5-* 7+");
}

TEST(ConvertToPostfix, can_convert_an_expression_with_real_nums)
{
	EXPECT_EQ(ConvertToPostfix("(1+2)/(3+4*6.7)-5.3*4.4"), "1 2+ 3 4 6.7*+/ 5.3 4.4*-");
}


TEST(ConvertToPostfix, can_convert_an_expression_with_many_digit_numbers)
{
	EXPECT_EQ(ConvertToPostfix("100.00000+20000/3500.01*(1000+21000000.123)"), "100.00000 20000 3500.01/ 1000 21000000.123+*+");
}

TEST(ConvertToPostfix, not_covnert_an_exp_with_incrrt_placement_of_pars)
{
	ASSERT_ANY_THROW(ConvertToPostfix("2*(8-5)+4)"));
}

TEST(Calculation, can_calculate_a_simple_expression)
{
	double res = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
	EXPECT_DOUBLE_EQ(Calculation("(1+2)/(3+4*6.7)-5.3*4.4"), res);
}

TEST(Calculation, can_calculate_an_expression_with_many_digit_numbers)
{
	double res = 100.0 + 20000.120 / 2500.0*(1000.0 + 20100000.0008);
	EXPECT_DOUBLE_EQ(Calculation("100.0+20000.120/2500.0*(1000.0+20100000.0008)"), res);
}

TEST(Calculation, not_calculate_an_exp_with_incorrect_placement_of_pars)
{
	ASSERT_ANY_THROW(Calculation("(1+2)(/())3+4*6.7)-5.3*4.4)("));
}
#endif