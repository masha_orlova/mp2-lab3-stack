#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack: public TDataRoot
{
private:
	int Hi;		//������ ���������� �������� �����
public:
	TStack(int Size = DefMemSize): TDataRoot(Size), Hi(-1) {}
	virtual void  Put(const TData &val);	//�������� �������
	virtual TData Get();					//����� �������
	TData Top();							//�������� ���������� ��-�� �����

	//��������� ������
	virtual int IsValid() {return 0;};		//������������ ���������
	virtual void Print();					//������ �������� �����
};

#endif