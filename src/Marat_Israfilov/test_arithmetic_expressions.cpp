#include "TStack.h"
#include "arithmetic_expressions.h"
#include <string>

#include "gtest.h"

TEST(arithmetic_expressions, can_check_symbols)
{
	string str = "(1+2)";
	EXPECT_TRUE(Check_Symbols(str));
}

TEST(arithmetic_expressions, symbols_checked_correctly)
{
	string str = "(1+2f)";
	EXPECT_FALSE(Check_Symbols(str));
}

TEST(arithmetic_expressions, can_check_brackets)
{
	string str = "(1+2)";
	EXPECT_TRUE(Check_Brackets(str));
}

TEST(arithmetic_expressions, brackets_checked_correctly)
{
	string str = "(1+2";
	EXPECT_FALSE(Check_Brackets(str));
}

TEST(arithmetic_expressions, conversion_to_postfix_form_is_correctly)
{
	string str = "(1+2)/(3*4)-5";
	EXPECT_EQ("1 2 +3 4 */5 -", Postfix_Conversion(str));
}

TEST(arithmetic_expressions, calculating_is_correctly)
{
	string str = "(1+2)/(3*4)-5";
	EXPECT_EQ(-4.75, Calculation(str));
}