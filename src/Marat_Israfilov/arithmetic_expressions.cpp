#include "arithmetic_expressions.h"

bool Check_Symbols(string InputString)
{
	bool result = true;
	for (string::iterator it = InputString.begin(), end = InputString.end(); it != end; ++it)
	{				
		if (*it >= '0' && *it <= '9' || *it == '.' || *it == ' ' || IsOperation(*it))
		{
			
		}
		else
		{
			result = false;
			break;
		}
	}
	return result;
}

bool Check_Brackets(string InputString) 
{	
	if (!Check_Symbols(InputString)) throw 1; //Error: invalid symbols!
	TStack st;
	int i = 1, errors = 0;
	bool result = true;


	for (string::iterator it = InputString.begin(), end = InputString.end(); it != end; ++it)
	{

		if (*it == '(')
		{
			st.Put((TData)i);
			++i;
		}
		else if (*it == ')')
		{
			if (st.IsEmpty())
			{
				cout << "- " << i << endl;
				++errors;
				result = false;
			}
			else
				cout << (int)st.Get() << " " << i << endl;
			++i;
		}
	}
	while (!st.IsEmpty())
	{
		cout << (int)st.Get() << " -" << endl;
		++errors;
		result = false;
	}
	cout << "Total number of errors = " << errors << endl;
	return result;
}

int Operation_Number(char InChar)
{
	if (InChar == '(')
		return 0;
	if (InChar == ')')
		return 1;
	if (InChar == '+' || InChar == '-')
		return 2;	
	if (InChar == '*' || InChar == '/')
		return 3;	
}

bool IsOperation(char InChar)
{
	if (InChar == '(' || InChar == ')' || InChar == '+' || InChar == '-' || InChar == '*' || InChar == '/')
		return 1;
	else
		return 0;
}

string Postfix_Conversion(string InputString)
{
	if (!Check_Brackets(InputString)) throw 2; //Error: not all brackets!		
	string OutputString;
	TStack tmp;
	int i = 0;

	for (string::iterator it = InputString.begin(), end = InputString.end(); it != end; ++it, ++i)
	{			
		if (IsOperation(*it))
		{
			if (tmp.IsEmpty() || Operation_Number(*it) == 0 || Operation_Number(*it) > Operation_Number(tmp.Current_Element()))
				tmp.Put(*it);
			else if (*it == ')')
			{
				while (tmp.Current_Element() != '(')
					OutputString = OutputString + (char)tmp.Get();
				tmp.Get();
			}
			else
			{
				while (!tmp.IsEmpty() && Operation_Number(*it) <= Operation_Number(tmp.Current_Element()))
				{
					if (tmp.Current_Element() == '(') tmp.Get();
					OutputString = OutputString + (char)tmp.Get();
				}
				tmp.Put(*it);
			}
		}
		else
			if (InputString[i + 1] == '.' || InputString[i] == '.' || (InputString[i + 1] >= '0' && InputString[i + 1] <= '9'))
				OutputString = OutputString + *it;
			else	
				OutputString = OutputString + *it + ' ';
	}	

	while (!tmp.IsEmpty())
		OutputString = OutputString + (char)tmp.Get();	

	return OutputString;
}

TData Calculation(string InputString)
{
	TStack st;
	TData result, tmp1, tmp2;
	string Postfix_Str = Postfix_Conversion(InputString);
	string var = "";

	for (string::iterator it = Postfix_Str.begin(), end = Postfix_Str.end(); it != end; ++it)
	{				
		if (*it >= '0' && *it <= '9' || *it == '.')								
			var += *it;		
		else if(!IsOperation(*it))
		{					
			st.Put(stod(var));
			var = "";			
		}
		else
		{					
			tmp2 = st.Get();
			tmp1 = st.Get();			
			
			if(*it == '+')			
				result = tmp1 + tmp2;
			else if(*it == '-')
				result = tmp1 - tmp2;
			else if (*it == '*')
				result = tmp1 * tmp2;
			else if (*it == '/')
				result = tmp1 / tmp2;		

			st.Put(result);
		}
	}

	return st.Get();
}
