#include "TStack.h"
#include <iostream>
using namespace std;

TStack::TStack(const TStack& CopyStack)
{
	MemSize = CopyStack.MemSize;
	DataCount = CopyStack.DataCount;
	MemType = CopyStack.MemType;
	pMem = new TElem[MemSize];
	for (int i = 0; i < MemSize; ++i)
	{
		pMem[i] = CopyStack.pMem[i];
	}
}

void TStack::Put(const TData& Val)
{
	if (pMem == nullptr) 
	{ 
		throw SetRetCode(DataNoMem); 
	}
	else if (IsFull())
	{
		void* p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		pMem[++Top] = Val;
		DataCount++;
	}
	else
	{
		pMem[++Top] = Val;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	if (pMem == nullptr)	
		throw SetRetCode(DataNoMem);	
	else if (IsEmpty())	
		throw SetRetCode(DataEmpty);	
	else
	{
		DataCount--;
		return pMem[Top--];
	}
}

void TStack::Print()
{
	if (DataCount == 0) { cout << "Stack is empty!"; }
	for (int i = 0; i < DataCount; ++i)
	{
		cout << pMem[i] << " ";
	}
	cout << endl;
}