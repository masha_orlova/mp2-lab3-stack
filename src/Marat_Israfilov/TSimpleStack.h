#ifndef __TSIMPLESTACK_H__
#define	__TSIMPLESTACK_H__
#define Size 25

template<class T>
class TSimpleStack
{
private:	
	int Top;
	T Mem[Size];
public:
	TSimpleStack() { Top = -1; };
	TSimpleStack(const TSimpleStack&);
	~TSimpleStack() {};
	bool IsEmpty() const { return Top == -1; };
	bool IsFull() const { return Top == (Size - 1); };
	void Push(const T&);
	T Pop();
	void Print() const;
};

template<class T>
TSimpleStack<T>::TSimpleStack(const TSimpleStack& CopyStack)
{
	Top = CopyStack.Top;
	for (int i = 0; i <= Top; ++i)
	{
		Mem[i] = CopyStack.Mem[i];
	}	
}

template<class T>
void TSimpleStack<T>::Push(const T& Elem)
{
	if (IsFull()) throw 1; //Error: Stack is full!
	Mem[++Top] = Elem;
}

template<class T>
T TSimpleStack<T>::Pop()
{
	if (IsEmpty()) throw 2; //Error: Stack is empty!
	return Mem[Top--];
}

template<class T>
void TSimpleStack<T>::Print() const
{
	if (IsEmpty()) throw 2; //Error: Stack is empty!
	for (int i = 0; i <= Top; ++i)
	{
		cout << Mem[i] << " ";
	}
	cout << endl;
}
#endif