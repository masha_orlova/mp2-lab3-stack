#include <iostream>
#include <iomanip>
#include <stdlib.h>  

#include "tdatstack.h"
#include "tsimplestack.h"
#include "experssion.h"

int checkBrackets(const char* str) 
{
	TStack stack(255);
	int bracketCounter = 0;
	int errors = 0;
	std::cout << " Brackets " << std::endl;	
	std::cout << "Open | Close " << std::endl;
	std::cout << "-----+------" << std::endl;
	while(char ch = *str++) {
		if (ch == '(') stack.Put(++bracketCounter);
		if (ch == ')') {
			++bracketCounter;
			if (stack.IsEmpty()) {
				stack.Put(0);
				++errors;
				std::cout << "  -  |  " << bracketCounter << std::endl;
			} else {
				std::cout << std::setw(3) << stack.Get() << "  |  " << bracketCounter << std::endl;
			}		
		}	
	}
	while (!stack.IsEmpty()) {
		int num = stack.Get();
		if (num != 0) {
			std::cout << std::setw(3) << num << "  |  -" << std::endl;
			++errors;
		}
	}
	return errors;
}

int opPrior(const char op)
{
	switch (op) {
		case '(': return 0;
		case ')': return 1;
		case '-': 
		case '+': return 2;
		case '*': 
		case '/': return 3;
	}
	return -1;
}

bool isOp(const char op)
{
	switch (op) {
		case '(': 
		case ')': 
		case '-': 
		case '+': 
		case '*': 
		case '/': return true;
	}
	return false;
}

int onTop(TStack& stack)
{
	int elem = stack.Get();
	stack.Put(elem);
	return elem;
}

void makePostfix(const char* inStr, char* outStr)
{
	TStack stack(255);
	char ch;
	while(ch = *inStr++) {
		if (!isOp(ch))	{*outStr++ = ch; continue;}		
		if (ch == ')') 
			while(!stack.IsEmpty())
			{
				char ch = stack.Get();
				if (ch != '(') *outStr++ = ch;				
				else break;
			}
		else {
			*outStr++ = ' ';
			int prior = opPrior(ch);
			if (prior == 0 || stack.IsEmpty() || prior > opPrior(onTop(stack))) {
				stack.Put(ch); 
				continue;
			}
			while(!stack.IsEmpty() && opPrior(onTop(stack)) >= prior) {				
				*outStr++ = stack.Get();
			}
			stack.Put(ch);
		}
	}
	while(!stack.IsEmpty()) 
		*outStr++ = stack.Get();
	*outStr = '\0';
}

double calc(const char* statement)
{
	char number[255] = {0};
	char buf[255];
	makePostfix(statement, buf);
	char* expr = buf;
	CalcStack stack;
	while(*expr != 0) {
		// eat number
		int i = 0;
		if (!isOp(*expr) && *expr != ' ') {
			while(!isOp(*expr) && *expr != ' '){
				number[i++] = *expr++;
			}
			number[i] = '\0';
			stack.Put(atof(number));
		}
		if (isOp(*expr)) {
			double rhs = stack.Get();
			double lhs = stack.Get();
			if (*expr == '+')
				stack.Put(rhs+lhs);
			if (*expr == '-')
				stack.Put(lhs-rhs);
			if (*expr == '*')
				stack.Put(lhs*rhs);
			if (*expr == '/')
				stack.Put(lhs/rhs);
		}
		++expr;	
	}
return stack.Get();
}
