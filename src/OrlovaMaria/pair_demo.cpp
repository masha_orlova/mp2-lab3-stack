
#include <iostream>

#include "experssion.h"

using namespace std;

void print_errors(int number)
{
	if (number == 0) 
		cout << "There are no any errors with brackets" << endl;
	else
		cout << number << " errors found" << endl;
}

void main()
{
  int num_errors = 0;
  char ex1[] = "(a+b1)/2+6.5)*(4.8+sin(x)";
  std::cout << ex1 << std::endl;
  print_errors(checkBrackets(ex1));
  std::cout << "-----------------------------------------" << std::endl;
  char ex2[] = "((((";
  std::cout << ex2 << std::endl;
  print_errors(checkBrackets(ex2));
  std::cout << "-----------------------------------------" << std::endl;
  char ex3[] = ")(";
  std::cout << ex3 << std::endl;
  print_errors(checkBrackets(ex3));
  std::cout << "-----------------------------------------" << std::endl;
  char ex4[] = "(";
  std::cout << ex4 << std::endl;
  print_errors(checkBrackets(ex4));
  std::cout << "-----------------------------------------" << std::endl;
  char ex5[] = ")";
  std::cout << ex5 << std::endl;
  print_errors(checkBrackets(ex5));
  std::cout << "-----------------------------------------" << std::endl;
  char ex6[] = "(a+ ((b + c) + d))";
  std::cout << ex6 << std::endl;
  print_errors(checkBrackets(ex6));
}
