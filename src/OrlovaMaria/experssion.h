#ifndef __EXPRESSION_H__
#define __EXPRESSION_H__

int checkBrackets(const char*);
void makePostfix(const char* inStr, char* outStr);
double calc(const char* expr);



#endif // __EXPRESSION_H__