#include <gtest.h>

#include <tsimplestack.h>

TEST(TSimpleStack, can_create_stack)
{
	ASSERT_NO_THROW(TSimpleStack s1);
}

TEST(TSimpleStack, new_stack_is_empty)
{
	TSimpleStack s1;
	EXPECT_EQ(true, s1.IsEmpty());
}

TEST(TSimpleStack, can_push_elem_in_stack)
{
	TSimpleStack s1;
	int elem = 1;
	ASSERT_NO_THROW(s1.Put(elem));
}

TEST(TSimpleStack, stack_with_elem_isnt_empty)
{
	TSimpleStack s1;
	int elem = 1;
	s1.Put(elem);
	EXPECT_EQ(false, s1.IsEmpty());
}

TEST(TSimpleStack, cant_push_in_full_stack)
{
	TSimpleStack s1;
	int i;
	for (i = 0; i < SIMPLE_STACK_MAX_SIZE; i++)
		s1.Put(i);
	ASSERT_ANY_THROW(s1.Put(i + 1));
}


TEST(TSimpleStack, can_pop_elem_from_stack)
{
	TSimpleStack s1;
	int elem1 = 1, elem2;
	s1.Put(elem1);
	elem2 = s1.Get();
	EXPECT_EQ(elem1, elem2);
}

TEST(TSimpleStack, cant_pop_from_empty_stack)
{
	TSimpleStack s1;
	ASSERT_ANY_THROW(s1.Get());
}

TEST(TSimpleStack, pop_returns_last_pushed_elem)
{
	TSimpleStack s1;
	int elem1 = 1, elem2 = 2, res;
	s1.Put(elem1); s1.Put(elem2);
	res = s1.Get();
	EXPECT_TRUE((res = elem2) && (res != elem1));
}

TEST(TSimpleStack, full_stack_after_pop_isnt_full)
{
	TSimpleStack s1;
	int i;
	for (i = 0; i < SIMPLE_STACK_MAX_SIZE; i++)
		s1.Put(i);
	bool isf = s1.IsFull();
	int val = s1.Get();
	EXPECT_TRUE(isf != s1.IsFull());
}
