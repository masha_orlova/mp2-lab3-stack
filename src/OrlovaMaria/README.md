# Методы программирования 2: Стек

[![Build Status](https://travis-ci.org/UNN-VMK-Software/mp2-lab1-set.svg)][travis]

<!-- TODO
  -
-->

## Введение

Лабораторная работа направлена на практическое освоение динамической структуры данных **Стек**.  В качестве области приложений выбрана тема вычисления арифметических выражений, возникающей при трансляции программ на языке программирования высокого уровня в исполняемые программы.

При вычислении произвольных арифметических выражений возникают две основные задачи: проверка корректности введённого выражения и выполнение операций в порядке, определяемом их приоритетами и расстановкой скобок. Существует алгоритм, позволяющий реализовать вычисление произвольного арифметического выражения за один просмотр без хранения промежуточных результатов. Для реализации данного алгоритма выражение должно быть представлено в постфиксной форме. Рассматриваемые в данной лабораторной работе алгоритмы являются начальным введением в область машинных вычислений.


## Цели и задачи

В рамках лабораторной работы ставится задача разработки двух видов стеков:

- прстейшего, основанного на статическом массиве (класс `TSimpleStack`);
- более сложного, основанного на использовании динамической структуры (класс `TStack`).

С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. Сложность выражения ограничена только длиной строки.


Перед выполнением работы был получен проект-шаблон, содержащий следующее:

 - Интерфейсы классов `TDataCom` и `TDataRoot` (h-файлы)
 - Тестовый пример использования класса `TStack`
 

## В ходе выполнения работы были решены следующие задачи:


### 1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.

```C++
#define SIMPLE_STACK_MAX_SIZE 25
template <class ValueType>
class TSimpleStack_Universal
{	
public: 
	TSimpleStack_Universal() : top_(-1) {}
	bool IsEmpty(void) const 
	{
		return top_ == -1;
	}
	bool IsFull(void) const 
	{
		return (top_ == (SIMPLE_STACK_MAX_SIZE - 1));
	}
	
	void  Put(const ValueType &Val) 
	{
	if (IsFull())
			throw std::runtime_error("Stack is full");
		buf_[++top_] = Val;
	}
	ValueType Get(void) 
	{
		if (IsEmpty())
			throw std::runtime_error("Stack is empty");
		return buf_[top_--];
	}
private:
	int top_;
	ValueType buf_[SIMPLE_STACK_MAX_SIZE];
};

typedef TSimpleStack_Universal<int> TSimpleStack;
typedef TSimpleStack_Universal<double> CalcStack;
```
  
### 2. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
  
```C++
#include "tdataroot.h"

TDataRoot::TDataRoot(int Size):TDataCom(), MemSize(Size), DataCount(0)
{
	if (Size == 0) {
		pMem = 0;
		MemType = MEM_RENTER;
	} else {
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
	}
}

TDataRoot::~TDataRoot() 
{
	if(MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = 0;
}

void TDataRoot::SetMem(void *p, int Size)
{
	if(MemType == MEM_HOLDER)
		delete pMem; 
	MemType = MEM_RENTER;
	pMem = (TElem *)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty(void) const 
{
	return DataCount == 0;
}
bool TDataRoot::IsFull (void) const
{
	return DataCount == MemSize;
}
```

### 3. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.

```C++
#include "tdataroot.h"

class TStack: public TDataRoot
{
public:
  TStack(int Size = DefMemSize);
  virtual void  Put   (const TData &Val); // добавить значение
  virtual TData Get   (void); // извлечь значение

  // служебные методы
  virtual int  IsValid();                 // тестирование структуры
  virtual void Print();                 // печать значений
};

```

```C++
#include <iostream>
#include "tdatstack.h"


// MemSize == Size
// DataCount == 0 if Empty


TStack::TStack(int Size):TDataRoot(Size) {}

void  TStack::Put(const TData &Val)
{
	if (IsFull()) {
		SetRetCode(DataErr);
		return;
	}
	pMem[DataCount++] = Val;		
}
TData TStack::Get(void)
{
	if (IsEmpty()) {
		SetRetCode(DataErr);
		return 0;
	}
	return pMem[--DataCount];
}

  // ñëóæåáíûå ìåòîäû
int  TStack::IsValid() {
	if (pMem == 0) return SetRetCode(DataErr);
	if (MemSize < 0) return SetRetCode(DataErr);
	if (DataCount < 0 || DataCount > MemSize) return SetRetCode(DataErr);
	return SetRetCode(DataOK);
}
;                 
void TStack::Print() {
	for (int i = 0; i < DataCount; i++)
		std::cout << pMem[i] << ' ';
	std::cout << std::endl;
}
```

### 4. Разработка тестов для проверки работоспособности стеков.

![SimpleTests](./tests.png)

  
### 5. Реализация алгоритма проверки правильности введенного арифметического выражения.
  
```C++
int checkBrackets(const char* str) 
{
	TStack stack(255);
	int bracketCounter = 0;
	int errors = 0;
	std::cout << " Brackets " << std::endl;	
	std::cout << "Open | Close " << std::endl;
	std::cout << "-----+------" << std::endl;
	while(char ch = *str++) {
		if (ch == '(') stack.Put(++bracketCounter);
		if (ch == ')') {
			++bracketCounter;
			if (stack.IsEmpty()) {
				stack.Put(0);
				++errors;
				std::cout << "  -  |  " << bracketCounter << std::endl;
			} else {
				std::cout << std::setw(3) << stack.Get() << "  |  " << bracketCounter << std::endl;
			}		
		}	
	}
	while (!stack.IsEmpty()) {
		int num = stack.Get();
		if (num != 0) {
			std::cout << std::setw(3) << num << "  |  -" << std::endl;
			++errors;
		}
	}
	return errors;
}
```
  
### 6. Реализация алгоритмов разбора и вычисления арифметического выражения.
  
```C++
int opPrior(const char op)
{
	switch (op) {
		case '(': return 0;
		case ')': return 1;
		case '-': 
		case '+': return 2;
		case '*': 
		case '/': return 3;
	}
	return -1;
}

bool isOp(const char op)
{
	switch (op) {
		case '(': 
		case ')': 
		case '-': 
		case '+': 
		case '*': 
		case '/': return true;
	}
	return false;
}

int onTop(TStack& stack)
{
	int elem = stack.Get();
	stack.Put(elem);
	return elem;
}

void makePostfix(const char* inStr, char* outStr)
{
	TStack stack(255);
	char ch;
	while(ch = *inStr++) {
		if (!isOp(ch))	{*outStr++ = ch; continue;}		
		if (ch == ')') 
			while(!stack.IsEmpty())
			{
				char ch = stack.Get();
				if (ch != '(') *outStr++ = ch;				
				else break;
			}
		else {
			*outStr++ = ' ';
			int prior = opPrior(ch);
			if (prior == 0 || stack.IsEmpty() || prior > opPrior(onTop(stack))) {
				stack.Put(ch); 
				continue;
			}
			while(!stack.IsEmpty() && opPrior(onTop(stack)) >= prior) {				
				*outStr++ = stack.Get();
			}
			stack.Put(ch);
		}
	}
	while(!stack.IsEmpty()) 
		*outStr++ = stack.Get();
	*outStr = '\0';
}
```

```C++
double calc(const char* statement)
{
	char number[255] = {0};
	char buf[255];
	makePostfix(statement, buf);
	char* expr = buf;
	CalcStack stack;
	while(*expr != 0) {
		// eat number
		int i = 0;
		if (!isOp(*expr) && *expr != ' ') {
			while(!isOp(*expr) && *expr != ' '){
				number[i++] = *expr++;
			}
			number[i] = '\0';
			stack.Put(atof(number));
		}
		if (isOp(*expr)) {
			double rhs = stack.Get();
			double lhs = stack.Get();
			if (*expr == '+')
				stack.Put(rhs+lhs);
			if (*expr == '-')
				stack.Put(lhs-rhs);
			if (*expr == '*')
				stack.Put(lhs*rhs);
			if (*expr == '/')
				stack.Put(lhs/rhs);
		}
		++expr;	
	}
return stack.Get();
}
```

### 7. Обеспечение работоспособности тестов и примеров использования.
![Sample original](./sample.png)
![Pairing brackets](./pairs.png)
![Postfix](./postfix.png)
![Calculations](./calc.png)


