#include "tdataroot.h"

TDataRoot::TDataRoot(int Size):TDataCom(), MemSize(Size), DataCount(0)
{
	if (Size == 0) {
		pMem = 0;
		MemType = MEM_RENTER;
	} else {
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
	}
}

TDataRoot::~TDataRoot() 
{
	if(MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = 0;
}

void TDataRoot::SetMem(void *p, int Size)
{
	if(MemType == MEM_HOLDER)
		delete pMem; 
	MemType = MEM_RENTER;
	pMem = (TElem *)p;
	MemSize = Size;
}

bool TDataRoot::IsEmpty(void) const 
{
	return DataCount == 0;
}
bool TDataRoot::IsFull (void) const
{
	return DataCount == MemSize;
}