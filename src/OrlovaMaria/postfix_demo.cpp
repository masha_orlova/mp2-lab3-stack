
#include <iostream>

#include "experssion.h"

using namespace std;

void main()
{
	char ex6[] = "(a+b1)/2+6.5*(4.8+4)";
	char p[255];
	makePostfix(ex6, p);
	std::cout << ex6 << std::endl;
	std::cout << p << std::endl;

	char ex7[] = "(a+ ((b + c) + d))";
	makePostfix(ex7, p);
	std::cout << ex7 << std::endl;
	std::cout << p << std::endl;

	char ex8[] = "(1+2)/(3+4*6.7)-5.3*4.4";
	makePostfix(ex8, p);
	std::cout << ex8 << std::endl;
	std::cout << p << std::endl;
}
