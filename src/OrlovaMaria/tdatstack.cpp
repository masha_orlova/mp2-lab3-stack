#include <iostream>
#include "tdatstack.h"


// MemSize == Size
// DataCount == 0 if Empty


TStack::TStack(int Size):TDataRoot(Size) {}

void  TStack::Put(const TData &Val)
{
	if (IsFull()) {
		SetRetCode(DataErr);
		return;
	}
	pMem[DataCount++] = Val;		
}
TData TStack::Get(void)
{
	if (IsEmpty()) {
		SetRetCode(DataErr);
		return 0;
	}
	return pMem[--DataCount];
}

  // ��������� ������
int  TStack::IsValid() {
	if (pMem == 0) return SetRetCode(DataErr);
	if (MemSize < 0) return SetRetCode(DataErr);
	if (DataCount < 0 || DataCount > MemSize) return SetRetCode(DataErr);
	return SetRetCode(DataOK);
}
;                 
void TStack::Print() {
	for (int i = 0; i < DataCount; i++)
		std::cout << pMem[i] << ' ';
	std::cout << std::endl;
}