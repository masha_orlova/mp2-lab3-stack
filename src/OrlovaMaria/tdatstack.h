#include "tdataroot.h"

class TStack: public TDataRoot
{
public:
  TStack(int Size = DefMemSize);
  virtual void  Put   (const TData &Val); // �������� ��������
  virtual TData Get   (void); // ������� ��������

  // ��������� ������
  virtual int  IsValid();                 // ������������ ���������
  virtual void Print();                 // ������ ��������
};
