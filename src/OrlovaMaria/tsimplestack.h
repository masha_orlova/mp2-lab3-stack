#include <exception>

#define SIMPLE_STACK_MAX_SIZE 25
template <class ValueType>
class TSimpleStack_Universal
{	
public: 
	TSimpleStack_Universal() : top_(-1) {}
	bool IsEmpty(void) const 
	{
		return top_ == -1;
	}
	bool IsFull(void) const 
	{
		return (top_ == (SIMPLE_STACK_MAX_SIZE - 1));
	}
	
	void  Put(const ValueType &Val) 
	{
	if (IsFull())
			throw std::runtime_error("Stack is full");
		buf_[++top_] = Val;
	}
	ValueType Get(void) 
	{
		if (IsEmpty())
			throw std::runtime_error("Stack is empty");
		return buf_[top_--];
	}
private:
	int top_;
	ValueType buf_[SIMPLE_STACK_MAX_SIZE];
};

typedef TSimpleStack_Universal<int> TSimpleStack;
typedef TSimpleStack_Universal<double> CalcStack;