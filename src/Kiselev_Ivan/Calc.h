#include <iostream>
#include "tdatstack.h"
#include "tsimplestack.h"

#ifndef _other_function_
#define _other_function_

using namespace std;

bool stample_is_good(char* exp)			//�������� ���������� ������
{
	Stack<int> braces(10);
	int i = 0, nu_braces = 0, number = 1;
	while ((exp[i] != '\0') && (nu_braces >= 0))
	{
		if (exp[i] == brace_1)
		{
			nu_braces++;
			braces.PutElem(number);
			number++;
		}
		if (exp[i] == brace_2)
		{
			nu_braces--;
			if (nu_braces >= 0) cout << "open : " << braces.GetElem() << " close : " << number << endl;
			else cout << "no open ..  close : " << number << endl;
			number++;
		}
		i++;
	}
	if (nu_braces == 0)
		return true;
	else
		if (nu_braces > 0)
		{
			cout << "open : " << braces.GetElem() << " no close ." << endl;
			return false;
		}
		else return false;
}

int CharToInt(char* ch) //����������� ����� �����
{
	int i = 0;
	int res = 0;
	while ((ch[i] != '\0') && (ch[i] >= '0') && (ch[i] <= '9'))
	{
		i++;
	}
	int k = 1;
	i--;
	while (i >= 0)
	{
		res = (ch[i] - '0')*k + res;
		ch[i] = '\0';
		i--;
		k *= 10;
	}
	return res;
}

double CharToDouble(char* ch) //������ ������� ����������� ������ ������� �����
{
	double res = 0;
	int i = 0;
	double k = 0.1;
	while ((ch[i] != '\0') && (ch[i] >= '0') && (ch[i] <= '9'))
	{
		res += (ch[i] - '0')*k;
		ch[i] = '\0';
		i++;
		k /= 10;
	}
	return res;
}


int priority(char N)			//���������� ���������� ��������
{
	int code;
	switch (N)
	{
	case  '(': {code = 0; break; }
	case  ')': {code = 1; break; }
	case  '+':
	case  '-': {code = 2; break; }
	case  '*':
	case  '/': {code = 3; break; }
	default: code = 4; break;
	}
	return code;
}

double Calc(char* arr)		//�������
{
	Stack<char> symbol(10);
	Stack<double> numbers(10);
	int i = 0;
	while ((arr[i] != '\0') && (arr[i] != '\n'))
	{
		if ((arr[i] <= '9') && (arr[i] >= '0'))				//���� ��� �����
		{
			numbers.PutElem(Func_numb(arr, i));
		}

		else											//������
		{
			if (arr[i] != ' ')
			{
				switch (arr[i])
				{
				case brace_1:
					symbol.PutElem(arr[i]);
					i++;
					break;		//���� ��� '(' ���������� � ����

				case brace_2:
					while (symbol.LastElem() != brace_1)
					{
						Perform(numbers, numbers.GetElem(), numbers.GetElem(), symbol.GetElem());
					};
					symbol.GetElem();
					i++;		//���� ��� ')' ��������� �������� �� ��� ���, ���� � ����� �� ���������
					break;		// '(', ����� ������� ���� ������ �� �����(���������� ������)

				case minus:
					if ((i == 0) || ((arr[i - 1] == brace_1) || (arr[i - 1] == mult) || (arr[i - 1] == divide)))
					{
						i++;
						numbers.PutElem(Func_numb(arr, i)*(-1));
					}	//���� ��� ������������� ����� (-� , *-� , /-�
					else	//����� ��������� ��� ��, ��� � � ������
					{
						if ((priority(arr[i]) > priority(symbol.GetElem())) || (symbol.GetTop() == -1)) symbol.PutElem(arr[i]);
						else
						{
							while ((priority(symbol.LastElem()) >= priority(arr[i])) && (symbol.GetTop() != -1))
							{
								Perform(numbers, numbers.GetElem(), numbers.GetElem(), symbol.GetElem());
							}
							symbol.PutElem(arr[i]);
						}
						i++;
					}
					break;


				case plus:
				case mult:
				case divide:
					if ((priority(arr[i]) > priority(symbol.LastElem())) || (symbol.GetTop() == -1))
						symbol.PutElem(arr[i]);//���� ��������� ������ �������� ������,��� ��, ��� � ����� ��� ���� ����. ����
					else				//����� ��������� �������� �� ���� �������, ���� /\ �� �����
					{
						while ((priority(symbol.LastElem()) >= priority(arr[i])) && (symbol.GetTop() != -1))
						{
							Perform(numbers, numbers.GetElem(), numbers.GetElem(), symbol.GetElem());
						}
						symbol.PutElem(arr[i]);	//����� ���� ��������� ��� ������� � ���� ��������
					}
					i++;
					break;

				default: exit(0);
					break;
				}
			}
			else  i++; 
		}

	}

	while (numbers.GetTop() > 0)
		Perform(numbers, numbers.GetElem(), numbers.GetElem(), symbol.GetElem());

	return numbers.GetElem();
}

void Perform(Stack<double> &st, double a, double b, char symb) //��������� ���� ��������
{
	switch (symb)
	{
	case plus: st.PutElem(a + b); break;
	case minus: st.PutElem(a - b); break;
	case mult: st.PutElem(a*b); break;
	case divide: st.PutElem(a / b); break;
	}
}

double Func_numb(char *arr, int &i)	//�������� �� ������ ����� � ����������� ��� � ����������� ������
{
	char *future_int;
	double res = 0;
	future_int = new char[50];
	int ogr = 0, symb_numb = 0;
	while ((arr[i] <= '9') && (arr[i] >= '0') && (ogr < 100))		//�� �� �����, ��� ��� ������� ������
	{
		future_int[symb_numb] = arr[i];
		i++;
		symb_numb++;
		ogr++;
	}
	symb_numb = 0;
	res += CharToInt(future_int);
	if ((arr[i] == '.') || (arr[i] == ','))
	{
		ogr = 0;
		i++;
		while ((arr[i] <= '9') && (arr[i] >= '0') && (ogr < 100))
		{
			future_int[symb_numb] = arr[i];
			i++;
			symb_numb++;
			ogr++;
		}
		res += CharToDouble(future_int);
	}
	return res;
}

#endif 

