#include "gtest.h"
#include "TSimpleStack.h"

TEST(TSimpleStack, can_create_simple_stack_with_positive_length)
{
	ASSERT_NO_THROW(TSimpleStack<int> A);
}
TEST(TSimpleStack, can_put_any_value_to_simple_stack)
{
	TSimpleStack<int> A;
	TSimpleStack<char> B;
	ASSERT_NO_THROW(A.Put(1));
	ASSERT_NO_THROW(B.Put('C'));
}
TEST(TSimpleStack, can_get_any_value_from_stack)
{
	TSimpleStack<int> A;
	TSimpleStack<char> B;
	A.Put(1);
	B.Put('C');
	EXPECT_EQ(1, A.Get());
	EXPECT_EQ('C', B.Get());
	EXPECT_EQ(1, A.IsEmpty());
}
TEST(TSimpleStack, cant_put_value_to_overflow_simple_stack)
{
	TSimpleStack<int> A;
	for (int i = 0; i < DefStackSize; i++)
		A.Put(2);
	ASSERT_ANY_THROW(A.Put(2));
}
TEST(TSimpleStack, cant_get_any_value_from_empty_simple_stack)
{
	TSimpleStack<int> A;
	ASSERT_ANY_THROW(A.Get());
}
TEST(TSimpleStack, can_copied_simple_stack)
{
	TSimpleStack<int> A;
	A.Put(100);
	TSimpleStack<int> B(A);
	EXPECT_EQ(100, B.Get());
	EXPECT_EQ(0, A.IsEmpty());
}
