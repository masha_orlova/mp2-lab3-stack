//  ���� ������������� ������ �� �������

#include <iostream>

#define DefStackSize 50									//������ �����

using namespace std;

template <class ValType>
class TSimpleStack
{
protected:                                   
	int p;                                              // ��������� �� ��������� �������
	ValType pStack[DefStackSize];                               // ������ �����
public:
	TSimpleStack() { p = 0; }
	TSimpleStack(const TSimpleStack<ValType> &s);  //������������ �����������
	bool IsEmpty() { return (p == 0); }            //�������� �������
	bool IsFull() { return (p == DefStackSize); }          //�������� �������
	int GetP() { return p; }                       //������� ��������� �� ��������� �������
	void Put(ValType val);                         //�������� �������
	ValType Get();                                 //����� �������
	void pop_back() { p--; }                       //������� ��������� �������
	friend ostream& operator<<(ostream &out, const TSimpleStack &s) //�����
	{
		for (int i = 0; i < s.p; i++)
			out << s.pStack[i] << " ";
		return out;
	}
};

template <class ValType>
TSimpleStack<ValType>::TSimpleStack(const TSimpleStack<ValType> &s)
{
	p = s.p;
	for (int i = 0; i < DefStackSize; i++)
		pStack[i] = s.pStack[i];
}

template <class ValType>
void TSimpleStack<ValType>::Put(ValType val)
{
	if (IsFull()) throw "stack overflow";
	pStack[p++] = val;
}

template <class ValType>
ValType TSimpleStack<ValType>::Get()
{
	if (IsEmpty()) throw "stack is blank";
	return pStack[--p];
}