// TStack - ������������ ���� "��������������" ������

#include "tdataroot.h"
#include "TSimpleStack.h"
#include <iostream>

#define STDDATASETMEM 25

using namespace std;

template <class ValType>
class TStack : public TDataRoot<ValType>
{
protected:
	void Reconstruction(int Size);
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) {};
	TStack(const TStack &s);          // ����������� �����������
	void Put(const ValType &val) { if (IsFull()) Reconstruction(STDDATASETMEM); pMem[DataCount++] = val; }; // �������� �������
	ValType Get() { return (IsEmpty()) ? (throw SetRetCode(DataEmpty)) : pMem[--DataCount]; };   // ������� �������
	void pop_back() { DataCount-- };
	int IsValid();              // ������������ ��������� 
	void Print();               // ������ ��������
};


template <class ValType>
TStack<ValType>::TStack(const TStack &s) : TDataRoot(s.Size), Top(s.Top)
{
	for (int i = 0; i < MemSize; i++)
		pMem[i] = s.pMem[i];
}

template <class ValType>
void TStack<ValType>::Reconstruction(int Size)
{
	if (MemType == MEM_HOLDER)
	{
		ValType *Temp;
		Temp = new ValType[MemSize + Size];
		for (int i = 0; i < MemSize; i++)
			Temp[i] = pMem[i];
		delete[] pMem;
		pMem = Temp;
		MemSize += Size;
	}
	else
		throw SetRetCode(DataFull);
}

template <class ValType>
int TStack<ValType>::IsValid()
{
	if (pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0) 
		return 0;
	return 1;
}

template <class ValType>
void TStack<ValType>::Print()
{
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;
}