#ifndef __TSIMPLESTACK_H__
#define __TSIMPLESTACK_H__

#define DEFAUT_SIZE 100

template <class data_type = int>
class TSimpleStack {
private:
	int top, size;
	data_type data[size];

public:
	TSimpleStack(int = DEFAUT_SIZE);
	TSimpleStack(const TSimpleStack&);
	void push(const data_type& Val);
	data_type pop();
	void print();
};

template <class data_type>
TSimpleStack<data_type>::TSimpleStack(int s): top(-1) {
	
	if (s < 0) {
		throw 1;
	}

	size = s;
	data = new data_type[size];

}

template <class data_type>
TSimpleStack<data_type>::TSimpleStack(const TSimpleStack& st) {

	top = st.top;
	size = st.size;

	data = new data_type[size];

	for (int i = 0; i <= top; i++) {
		data[i] = st.data[i];
	}

}

template <class data_type>
void TSimpleStack<data_type>::push(const data_type& Val) {
	
	if (top == size - 1) {
		throw 2;
	}

	data[++top] = Val;

}

template <class data_type>
data_type TSimpleStack<data_type>::pop() {

	if (top == -1) {
		throw 3;
	}

	return data[top--];

}

template <class data_type>
void TSimpleStack<data_type>::print() {

	for (int i = 0; i <= Top; i++)
		cout << data[i] << ' ';
	cout << endl;

}

#endif