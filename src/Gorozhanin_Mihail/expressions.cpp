#include <iostream>
#include <cstdlib>
#include "tstack.h"
#include "expressions.h"

using namespace std;

int GetOperationPrio(char c)
{
	if (c == '(')
		return 0;
	else if (c == ')')
		return 1;
	else if (c == '+' || c == '-')
		return 2;
	else if (c == '*' || c == '/')
		return 3;
	else
		return -1;
}

double CalcOfExpr(string expr)
{
	TStack<double> st(20, 10);
	int size = expr.length(), pos = 0;
	double op1, op2;
	char ch, operandStr[20];

	if (size <= 0)
		throw 3;

	expr = InfixToPolish(expr);
	size = expr.length();

	for (int i = 0; i < size; i++)
	{
		ch = expr[i];

		if (ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == ' ')
			if (operandStr[0] != '\0')
			{
				st.Put(atof(operandStr));
				pos = 0;
				operandStr[0] = '\0';
			}

		if (ch == '+')
		{
			op1 = st.Get();
			op2 = st.Get();
			st.Put(op2 + op1);
		}
		else if (ch == '-')
		{
			op1 = st.Get();
			op2 = st.Get();
			st.Put(op2 - op1);
		}
		else if (ch == '*')
		{
			op1 = st.Get();
			op2 = st.Get();
			st.Put(op2 * op1);
		}
		else if (ch == '/')
		{
			op1 = st.Get();
			op2 = st.Get();
			st.Put(op2 / op1);
		}
		else if (ch != ' ')
		{
			operandStr[pos++] = ch;
			operandStr[pos] = '\0';
		}

	}
	return st.Get();
}

string InfixToPolish(string infixExpr)
{
	TStack<char> st(20, 10);
	bool IsOperand = false;
	int pos = 0, size = infixExpr.length();
	char ch, temp;
	string polishExpr = "";

	for (int i = 0; i < size; i++)
	{
		ch = infixExpr[i];

		if (ch == '(')
			st.Put(ch);
		else if (ch == ')')
			while (1)
			{
				temp = st.Get();
				if (temp == '(')
					break;
				polishExpr += temp;
			}
		else if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
		{
			polishExpr += ' ';
			if (st.IsEmpty())
				st.Put(ch);
			else
			{
				temp = st.Get();
				if (GetOperationPrio(ch) > GetOperationPrio(temp))
				{
					st.Put(temp);
					st.Put(ch);
				}
				else
				{
					polishExpr += temp;
					while (!st.IsEmpty())
					{
						temp = st.Get();
						if (GetOperationPrio(temp) >= GetOperationPrio(ch))
							polishExpr += temp;
						else
						{
							st.Put(temp);
							break;
						}
					}
					st.Put(ch);
				}
			}
		}
		else
		{
			IsOperand = true;
			polishExpr += ch;
		}
	}

	while (!st.IsEmpty())
		polishExpr += st.Get();

	return polishExpr;
}

int IsCorrect(string expr)
{
	TStack<int> st(20, 10);
	int size = expr.length();
	int numOfErrors = 0, bracketNum = 0;


	if (size <= 0)
		throw 1;

	cout << "Brackets" << endl;
	cout << "Opening" << " " << "Closing" << endl;

	for (int i = 0; i < size; i++)
	{
		if (expr[i] == '(')
			st.Put(++bracketNum);
		if (expr[i] == ')')
		{
			bracketNum++;

			if (st.IsEmpty())
			{
				numOfErrors++;
				cout << '-' << "       " << bracketNum << endl;
			}
			else
				cout << st.Get() << "       " << bracketNum << endl;
		}
	}

	while (!st.IsEmpty())
	{
		cout << st.Get() << "       " << '-' << endl;
		numOfErrors++;
	}

	cout << "Number of errors: " << numOfErrors << endl;

	return numOfErrors;
}